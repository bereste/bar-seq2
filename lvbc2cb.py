#!/usr/bin/env python

'''
Convert BAR-seq output from LVBC to CB (and viceversa)
'''

import argparse
import logging
import os
import sys
import numpy as np

def main():
    parser = argparse.ArgumentParser(
        description = "convert LVBC to CB",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-i", "--input", help = "Output of BAR-seq with LVBC.",
                        required = True, dest = "input_file")
    parser.add_argument("-melt", "--melt", help = "Melt output format.",
                        dest = "melt", action = "store_true")
    parser.add_argument('-v', '--verbose', help='increase output verbosity',
        action='count', default=0)

    args = parser.parse_args()
    if args.verbose == 0:
        log_level = logging.INFO
    elif args.verbose == 1:
        log_level = logging.DEBUG
    else:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level,
                        format='%(levelname)-8s [%(asctime)s]  %(message)s',
                        datefmt="%y%m%d %H%M%S")

    logging.info("Program Started")
    if not os.path.exists(args.input_file):
        logging.error("Input file '%s' not found!", args.input_file)
    cb_dict = {}
    bc_len = 0
    h = "#"
    with open(args.input_file, "r") as input_file:
        h = input_file.readline().rstrip().split()
        for line in input_file.readlines():
            bc,count,perc,cbs,cbscount = line.rstrip().split()
            bc = bc.replace("\"", "")
            cbs_split = cbs.split(",")
            bc_len = len(cbs_split[0])
            cbscount_split = cbscount.split(",")
            for cb in range(0, len(cbs_split)):
                if cbs_split[cb] not in cb_dict:
                    cb_dict[cbs_split[cb]] = {}
                if bc not in cb_dict[cbs_split[cb]]:
                    cb_dict[cbs_split[cb]][bc] = 0
                cb_dict[cbs_split[cb]][bc] += int(cbscount_split[cb])
    logging.info("Found %d CB", len(cb_dict))
    dtype = [('cb', np.unicode_, bc_len), ('count', int)]
    vals = []
    tot_count = 0
    for cb in cb_dict:
        cb_count = sum(cb_dict[cb].values())
        vals.append((cb, cb_count))
        tot_count += cb_count
    npa = np.array(vals, dtype = dtype)
    npa = np.sort(npa, order = 'count')[::-1]
    sum_counts_cumul = 0
    ss = 0
    h_new = []
    for h_lab in h:
        if h_lab.endswith("-Barcode"):
            h_new.append(h_lab.replace("-Barcode", "-CB"))
            continue
        if h_lab.endswith("-CBs") or h_lab.endswith("-CBCounts"):
            h_new.append(h_lab.replace("-CB", "-Barcode"))
            continue
        h_new.append(h_lab)
    print("\t".join(h_new))
    for npa_elem in npa:
        cb = npa_elem['cb']
        sum_counts_cumul += npa_elem['count']
        ss = (sum_counts_cumul / tot_count) * 100
        vec_out = []
        if args.melt:
            for cbs in cb_dict[cb].keys():
                vec_out.append([cb, str(npa_elem['count']), "{:.2f}".format(round(ss, 2)), cbs, str(cb_dict[cb][cbs])])
        else:
            cbs = [x for x in cb_dict[cb].keys()]
            cbs_counts = [str(cb_dict[cb][x]) for x in cbs]
            vec_out.append([cb, str(npa_elem['count']), "{:.2f}".format(round(ss, 2)), ",".join(cbs), ",".join(cbs_counts)])
        for vo in vec_out:
            print("\t".join(vo))
    logging.info("Program Completed!")

if __name__ == "__main__":
    main()
