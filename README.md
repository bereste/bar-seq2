# BAR-Seq2 #

New version (v2) of the BAR-Seq software for the analysis of Homology-Directed Repair barcoded cells.

The orginal version of the BAR-Seq software is still available [here](http://www.bioinfotiget.it/barseq) and its source code [here](https://bitbucket.org/bereste/bar-seq).

### Changes ###

The main changes of BAR-Seq2 (w.r.t. the original version) are in the graph-based procedure for barcode correction, allowing a more robust detection of low-count ones, and the possibility to analyze 10X single-cell data, by exploiting cellular barcodes and umi.
Other improvements are the integartion of cutadapt to offer the possibility of performing read pre-processing, the automatic detection of the amplicon structure which is then used by TagDust to extract barcodes, and finally some refinements in the data structures and output files.

---
### Download and Installation ###

BAR-Seq2 is based on [TagDust](http://tagdust.sourceforge.net/) for the extraction of barcodes from NGS reads.
A copy of the TagDust software is present in the `ext-progs` folder.

To install TagDust follow the instructions provided [here](http://tagdust.sourceforge.net/#install).
The BAR-Seq pipeline was tested with TagDust v2.33.

### Requirements ###

BAR-Seq2 is based on python 3 (version 3.8 or higher) and requires the following packages (with the suggested versions):

* pandas (v1.4.1)
* numpy (v1.19.2)
* editdistance (v0.5.3)
* networkx (v2.8)
* matplotlib (v.3.5.1)
* logomaker (v0.8)
* scipy (v1.8.0)
* umi_tools (v1.1.2)

Also, this new version includes the trimming step of the input read using [Cutadapt](https://cutadapt.readthedocs.io)(v3.5).

---
### Run ###

The BAR-Seq2 pipeline was developed on a Linux 64bit server, but works also on Mac OS X systems.

Usage:

```
python runBARseq.py [-h] -r1 IN_R1_FILES [-r2 IN_R2_FILES] [-bc BC_PATTERN] [-l IN_LABELS] [-o OUT_DIR]
                    [-wl WHITELIST] [--wl-knee-method {distance,density}] [-trim-sequences] [-trim-opt TRIM_OPT]
					[-tagdust-dir TAGDUST_DIR] [-tagdust-opt TAGDUST_OPT] [-f FILTER_STRUCT] [-iupac IUPAC_STRUCT]
					[-e ED_THR] [-merge-algorithm MERGE_ALGORITHM] [-c MIN_COUNT] [-t THREADS] [-save-graph] [-v]
```
where:

```
  -h, --help            show this help message and exit
  -r1 IN_R1_FILES, --r1-files IN_R1_FILES
                        Comma separated list of R1 files. (default: None)
  -r2 IN_R2_FILES, --r2-files IN_R2_FILES
                        Comma separated list of R2 files (single-cell mode). (default: )
  -bc BC_PATTERN, --bc-pattern BC_PATTERN
                        Barcode pattern (single-cell mode). (default: )
  -l IN_LABELS, --file-labels IN_LABELS
                        Comma separated list of file labels. (default: )
  -o OUT_DIR, --out-dir OUT_DIR
                        Output directory (must exist). (default: .)
  -wl WHITELIST, --whitelist WHITELIST
                        A whitelist of accepted cell barcodes (single-cell mode). (default: )
  --wl-knee-method {distance,density}, -wl-knee-method {distance,density}
                        Use distance or density methods for detection of knee in whitelist computation (single-cell mode). (default: density)
  -trim-sequences, --trim-sequences
                        Trim input R2 sequences with 'cutadapt'. (default: False)
  -trim-opt TRIM_OPT, --trim-opt TRIM_OPT
                        Cutadapt parameters used to trim input R2 sequences. (default: )
  -tagdust-dir TAGDUST_DIR, --tagdust-dir TAGDUST_DIR
                        TagDust program directory. (default: )
  -tagdust-opt TAGDUST_OPT, --tagdust-opt TAGDUST_OPT
                        TagDust run options (e.g. "-1 P:CAGGG -2 R:N -3 S:TAGGGACAGGA -4 P:AAGCCTCCTCCT"). (default: )
  -f FILTER_STRUCT, --filter-struct FILTER_STRUCT
                        Structural filter: 'nofilter, 'percfilter', or 'fixedstruct'. (default: percfilter)
  -iupac IUPAC_STRUCT, --iupac-struct IUPAC_STRUCT
                        IUPAC structure used only with 'fixedstruct' filter. (default: N)
  -e ED_THR, --edit-distance ED_THR
                        Edit distance threshold for graph-based merging. (default: 2)
  -merge-algorithm MERGE_ALGORITHM, --merge-algorithm MERGE_ALGORITHM
                        Graph Algorithm for Barcode mergins: 'communities' (new) or 'ego' (old). (default: communities)
  -c MIN_COUNT, --min-count MIN_COUNT
                        Minimum count threshold for an input barcode. (default: 3)
  -t THREADS, --threads THREADS
                        Num. of threads used in sequence comparisons. (default: 1)
  -save-graph, --save-graph
                        Save graph used to correct barcodes (GraphML format). (default: False)
  -v, --verbose         Increase verbosity. (default: 0)
```

The only required input is the R1 Fastq file (`-r1`), so the easier way to run BAR-Seq2 is:

```
python runBARseq.py -r1 <in_file.fq.gz>
```

### Output ###

For each input file BAR-Seq2 produces the following outputs:

* `<SAMPLE_NAME>.barcode.mc<MIN_COUNT>.tsv` list of barcodes having count at least `<MIN_COUNT>`
* `<SAMPLE_NAME>.barcode.saturation.png` plot of counts and saturation level of barcodes
* `<SAMPLE_NAME>.barcode.structure.png` logo plot of the barcode compositions
* (optional) `<SAMPLE_NAME>.graph_full.graphml` graph (based on edit distance) of the extracted barcodes

where `<SAMPLE_NAME>` is the label corresponding to the sample or (if not provided) it is extracted from the input FASTQ file.

If more than one input file is provided, BAR-Seq2 computes also the sharing results:

* `Barcode.mc<MIN_COUNT>_fullMatrix.tsv` matrix containing all the barcodes having count at least `<MIN_COUNT>` (rows) in all the provided samples (columns)

---
### Contacts ###
* Beretta Stefano
* Ivan Merelli
