import sys
import os
import argparse
import logging
import networkx as nx
import networkx.algorithms.community as nx_comm
import matplotlib.pyplot as plt

def main(raw_args = None):
    parser = argparse.ArgumentParser(prog = "graphAnalysis",
                                     description = "analysis of barcode graph.",
                                     formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-g", "--graph", help = "Graph file (.graphml).",
                        required = True, dest = "in_graph")
    parser.add_argument("-v", "--verbose", help = "Increase verbosity.",
                        action = "count", default = 0)
    args = parser.parse_args(raw_args)

    if args.verbose == 0:
        log_level = logging.INFO
    elif args.verbose == 1:
        log_level = logging.DEBUG
    else:
        log_level = logging.DEBUG

    logging.basicConfig(level = log_level,
                        format = "[%(asctime)s] %(levelname)-8s %(message)s",
                        datefmt = "%Y-%m-%d %H:%M:%S")
    logging.info("### Analysis Started ###")
    if not os.path.exists(args.in_graph):
        logging.error("Cannot find input file: %s", args.in_graph)
        return 1
    g = nx.read_graphml(path = args.in_graph)
    logging.info("Num. nodes: %d", g.number_of_nodes())
    logging.info("Num. edges: %d", g.number_of_edges())
    # Largest CC
    largest_cc = max(nx.connected_components(g), key = len)
    g_largest_cc = g.subgraph(largest_cc)
    c = nx_comm.greedy_modularity_communities(g_largest_cc, weight = "abundance")
    # color = ["red", "green", "blue", "orange", "purple", "pink", "grey"]
    # if len(c) > len(color):
    #     cmap = plt.get_cmap('tab20c')
    #     color = [cmap(i) for i in range(cmap.N)]
    # fig, ax1 = plt.subplots(figsize=(10,7))
    # #pos = nx.spring_layout(g_largest_cc)
    # pos = nx.nx_pydot.pydot_layout(g_largest_cc)
    # for comm_pos in range(0, len(c)):
    #     nx.draw_networkx_nodes(g_largest_cc, pos, node_size = 100, nodelist = c[comm_pos], node_color = color[comm_pos])
    # nx.draw_networkx_edges(g_largest_cc, pos, alpha = 0.5, width = 1)
    # plt.savefig("Test_largestCC.png")

    # Full G
    out_file = args.in_graph.replace(".graphml", ".png")
    fig, ax1 = plt.subplots(figsize=(30,20))
    logging.info("Computing graph layout")
    pos = nx.nx_pydot.pydot_layout(g)
    logging.info("Parsing CC")
    num_bcodes = 0
    for cc in nx.connected_components(g):
        g_cc = nx.subgraph(g, cc)
        if g_cc.number_of_nodes() > 1:
            color = ["red", "green", "blue", "orange", "purple", "pink", "grey"]
            g_cc_comm = nx_comm.greedy_modularity_communities(g_cc, weight = "abundance")
            if len(g_cc_comm) > len(color):
                cmap = plt.get_cmap('tab20')
                color = [cmap(i) for i in range(cmap.N)]
            for comm_pos in range(0, len(g_cc_comm)):
                if comm_pos < len(color):
                    nx.draw_networkx_nodes(g, pos, node_size = 10, nodelist = g_cc_comm[comm_pos], node_color = color[comm_pos])
                else:
                    nx.draw_networkx_nodes(g, pos, node_size = 10, nodelist = g_cc_comm[comm_pos], node_color = "black")
                num_bcodes += 1
        else:
            # nx.draw_networkx_nodes(g, pos, node_size = 20, nodelist = g_cc.nodes, node_color = "black")
            num_bcodes += 1
    nx.draw_networkx_edges(g, pos, alpha = 0.5, width = 1)
    plt.savefig(out_file)
    logging.info("Num. Barcodes (approx): %d", num_bcodes)
    logging.info("### Analysis Completed ###")

if __name__ == "__main__":
    main()
