#!/usr/bin/env python

import sys
import os
import argparse
import logging
import collections
import math
import gzip
import shutil
import random
import subprocess as sb
from textwrap import wrap
from multiprocessing.pool import Pool
from functools import partial

# Check required python package
def check_pkg(library_name):
    try:
        return __import__(library_name)
    except:
        logging.basicConfig(level = logging.INFO,
                            format = '[%(asctime)s] %(levelname)-8s %(message)s',
                            datefmt = "%Y-%m-%d %H:%M:%S")
        logging.error('Python package \'%s\' is missing!', library_name)
        sys.exit(1)

pd = check_pkg('pandas')
plt = check_pkg('pylab')
np = check_pkg('numpy')
ed = check_pkg('editdistance')
nx = check_pkg('networkx')
from networkx.algorithms.community import greedy_modularity_communities
matplotlib = check_pkg('matplotlib')
matplotlib.use('Agg')
import matplotlib.pyplot as plt
lm = check_pkg('logomaker')
from networkx.drawing.nx_agraph import graphviz_layout
check_pkg('umi_tools')
import umi_tools.extract_methods as extract_methods
import umi_tools.umi_methods as umi_methods
import umi_tools.whitelist_methods as whitelist_methods

# Check required external programs
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def check_program(binary_name):
    if not which(binary_name):
        logging.error('You need to install and have the command \'%s\' in your PATH variable!', binary_name)
        sys.exit(1)

#########################
### Utility Functions ###
#########################

## Parse FASTQ entry
def processFQ(lines = None):
    ks = ['name', 'sequence', 'optional', 'quality']
    return {k: v for k, v in zip(ks, lines)}

## Match barcode sequence based on template
def match_bc(seq, template):
  count = 0
  degenerate = {'A' : 'A', 'C' : 'C', 'G' : 'G', 'T' : 'T',
                'W' : 'AT', 'S' : 'CG', 'M' : 'AC', 'K' : 'GT', 'R' : 'AG', 'Y' : 'CT',
                'B' : 'CGT', 'D' : 'AGT', 'H' : 'ACT', 'V' : 'ACG', 'N':'ACGT'}
  for x in range(len(seq)):
    if not seq[x] in degenerate[template[x]]:
      count += 1
  return count

## Functions to retrived matrix cell position from 1-d array
def calc_row_idx(k, n):
    return int(math.ceil((1/2.) * (- (-8*k + 4 *n**2 -4*n - 7)**0.5 + 2*n -1) - 1))

def elem_in_i_rows(i, n):
    return i * (n - 1 - i) + (i*(i + 1))/2

def calc_col_idx(k, i, n):
    return int(n - elem_in_i_rows(i + 1, n) + k)

def condensed_to_square(k, n):
    i = calc_row_idx(k, n)
    j = calc_col_idx(k, i, n)
    return i, j

# Compute edit distances among sequences (in a range of positions) from a linearized matrix
def computeED(seq_array, range_pos):
    num_seqs = len(seq_array)
    dm = np.zeros((num_seqs * (num_seqs - 1)) // 2, dtype = np.int8)
    for i in range(range_pos[0], range_pos[1]):
        x,y = condensed_to_square(i, num_seqs)
        dm[i] = ed.eval(seq_array[x], seq_array[y])
    logging.debug("%d -> %d: %d", range_pos[0], range_pos[1], range_pos[1] - range_pos[0])
    return dm

# Split a range of values into 'num_split' sub-intevals
def splitRange(num_elem, num_split):
    int_size = int(num_elem / num_split)
    bsize = [int_size for i in range(0, num_split)]
    surplus = num_elem % num_split
    i = 0
    for ss in range(surplus, 0, -1):
        bsize[i] += 1
        i = (i+1) % num_split
    split_array = []
    k = 0
    for i in range(0, num_split):
        split_array.append([k, k + bsize[i]])
        k += bsize[i]
    return split_array

## Find and check TagDust program directory
def checkTagDustProg(td_dir):
    fpaths = []
    if td_dir != "":
        if not os.path.exists(td_dir):
            logging.error("Provided TagDust dir %s not found!", td_dir)
            return ""
        fpaths.append(td_dir)
    else:
        logging.info("TagDust dir not provided. Try to look for it...")
        fpaths.append(".")
        fp,fn = os.path.split(sys.argv[0])
        if fp != "" and fp not in fpaths:
            fpaths.append(fp)
    for fp in fpaths:
        for root,dnames,fnames in os.walk(fp):
            if "tagdust" in root:
                td_prog = os.path.join(root, "tagdust")
                if os.path.exists(td_prog):
                    logging.info("TagDust program found in: %s", root)
                    return td_prog
    return ""

####################################################
## open file in *filename* with mode *mode*.      ##
## If *create* is set, the directory containing   ##
## filename will be created if it does not exist. ##
## gzip - compressed files are recognized by the  ##
## suffix ``.gz`` and opened transparently.       ##
## returns a file or file-like object.            ##
####################################################
def openFile(filename, mode = "r", create_dir = False):
    _, ext = os.path.splitext(filename)

    if create_dir:
        dirname = os.path.dirname(filename)
        if dirname and not os.path.exists(dirname):
            os.makedirs(dirname)
    if ext.lower() in (".gz", ".z"):
        if sys.version_info.major >= 3:
            if mode == "r":
                return gzip.open(filename, 'rt', encoding="ascii")
            elif mode == "w":
                return gzip.open(filename, 'wt', compresslevel=6, encoding="ascii")
            else:
                raise NotImplementedError("mode '{}' not implemented".format(mode))
        else:
            return gzip.open(filename, mode, compresslevel=6)
    else:
        return open(filename, mode)

###########################################
## Guess amplicon structure for TagDust: ##
## samples each 'sampl_freq' reads of    ##
## infer nuclotide composition at each   ##
## position, and finds fixed structure.  ##
###########################################
def guessTagDustOpt(in_file, sampl_freq = 4, cov_thr = 0.9, nucl_thr = 0.9):
    logging.info("No options provided for TagDust. Try to infer from input.")
    num_read = 0
    num_sampled = 0
    seq_compos = [{'A' : 0, 'C' : 0, 'G' : 0, 'T' : 0, 'N' : 0} for k in range(100)]
    with openFile(in_file, 'r') as in_fq:
        lines = []
        for line in in_fq:
            lines.append(line.rstrip())
            if len(lines) == 4:
                record = processFQ(lines)
                if num_read % sampl_freq == 0:
                    num_sampled += 1
                    for p in range(0, len(record['sequence'])):
                        if len(seq_compos) < p:
                            seq_compos.append({'A' : 0, 'C' : 0, 'G' : 0, 'T' : 0, 'N' : 0})
                        seq_compos[p][record['sequence'][p]] += 1
                num_read += 1
                lines = []
    logging.info("Num. reads: %d", num_read)
    logging.info("Num. sampled reads: %d", num_sampled)
    nucl_cons = [max(p, key = p.get) for p in seq_compos if sum(p.values()) > 0]
    logging.debug(" ".join([x for x in nucl_cons]))
    nucl_freq = [max(p.values())/float(sum(p.values())) for p in seq_compos if sum(p.values()) > 0]
    logging.debug(" ".join([str(round(x, 2)) for x in nucl_freq]))
    nucl_freq = [1 if max(p.values())/float(sum(p.values())) > nucl_thr else 0 for p in seq_compos if sum(p.values()) > 0]
    logging.debug(" ".join([str(round(x, 2)) for x in nucl_freq]))
    if len(nucl_cons) != len(nucl_freq):
        logging.error("Error in computing amplicon structure!")
        return ""
    s = 0
    e = 0
    isZero = False
    nucl_inter = []
    for f in range(len(nucl_freq)):
        if nucl_freq[f] == 1:
            if isZero:
                s = e = f
                isZero = False
            e += 1
        else:
            if not isZero:
                nucl_inter.append([s,e])
            isZero = True
    if not isZero:
        nucl_inter.append([s,e])
    logging.debug(nucl_inter)
    if len(nucl_inter) < 2:
        logging.error("Cannot find upstream and/or downstream amplicon sequence(s).")
        return ""
    sel_inter = [nucl_inter[0], nucl_inter[1]]
    min_max = 0 if sel_inter[0][1] - sel_inter[0][0] < sel_inter[1][1] - sel_inter[1][0] else 1
    if len(nucl_inter) > 2:
        for inter in nucl_inter[2:]:
            if inter[1] - inter[0] > sel_inter[min_max][1] - sel_inter[min_max][0]:
                sel_inter[min_max] = inter
                min_max = 0 if sel_inter[0][1] - sel_inter[0][0] < sel_inter[1][1] - sel_inter[1][0] else 1
    up_inter = 0 if sel_inter[0][0] < sel_inter[1][0] else 1
    down_inter = 1 - up_inter
    td_opt_up = [nucl_cons[pos] for pos in range(sel_inter[up_inter][0], sel_inter[up_inter][1])]
    td_opt_down = [nucl_cons[pos] for pos in range(sel_inter[down_inter][0], sel_inter[down_inter][1])]
    logging.debug(td_opt_up)
    logging.debug(td_opt_down)
    td_opt_down_anchor = td_opt_down
    if len(td_opt_down) >= 10:
        td_opt_down_anchor = td_opt_down[0:11]
        td_opt_down = td_opt_down[11:]
    td_opt = "-1 P:" + "".join(td_opt_up) + " -2 R:N " + "-3 S:" + "".join(td_opt_down_anchor)
    if len(td_opt_down) > 0:
        td_opt += " -4 P:" + "".join(td_opt_down)
    return td_opt

######################################
## Trim input R2 file with Cutadapt ##
######################################
def trim_reads(r1_file, r2_file, trim_opt, out_dir, sample, r1_pattern, sc_mode, num_threads = 1):
    logging.info("Trimming read in %s with 'cutadapt'.", r2_file)
    out_r1_file = out_dir + "/" + sample + "_R1_trim.fastq.gz"
    out_r2_file = out_dir + "/" + sample + "_R2_trim.fastq.gz"
    log_file = out_dir + "/" + sample + "_cutadapt.log"
    # Check if provided options could be in conflict
    core_str = ["-j", "--cores", "--pair-filter", "--minimum-length", "-m"]
    if not sc_mode:
        core_str = ["-j", "--cores", "--pair-filter"]
    for cs in core_str:
        cs_pos = trim_opt.find(cs)
        if cs_pos != -1:
            repl_str = cs
            logging.warning("Replacing CutAdapt '%s' option", cs)
            p = cs_pos + len(cs)
            if trim_opt[p] == " " or trim_opt[p] == "=":
                repl_str += trim_opt[p]
            p += 1
            while(p < len(trim_opt) and trim_opt[p] != " "):
                repl_str += trim_opt[p]
                p += 1
            trim_opt = trim_opt.replace(repl_str, "")
    trim_cmd = "cutadapt " + trim_opt + \
      " --pair-filter=any -o " + out_r2_file + \
      " -p " + out_r1_file + \
      " -m " + str(len(r1_pattern)) + \
      " -j " + str(num_threads) + \
      " " + r2_file + " " + r1_file + " &> " + log_file
    if not sc_mode:
        trim_cmd = "cutadapt " + trim_opt + \
          " -o " + out_r1_file + \
          " -j " + str(num_threads) + \
          " " + r1_file + " &> " + log_file
    logging.debug(trim_cmd)
    CUTADAPT_STATUS = sb.call(trim_cmd, shell = True)
    if CUTADAPT_STATUS or not os.path.exists(out_r1_file):
        logging.error("Cutadapt failed to run, please check the log file.")
        sys.exit(1)
    logging.info("Read Trimming Completed!")
    return out_r1_file, out_r2_file

################################################################
## Compute cellular barcode whitelist to be used in extracUMI ##
################################################################
def computeWhitelist(in_r1, bc_pattern, out_r2, whitelist_knee_method, ed_above_threshold = None):
    read1s = umi_methods.fastqIterate(openFile(in_r1))
    # set up read extractor
    ReadExtractor = extract_methods.ExtractFilterAndUpdate(
        method = "string",
        pattern = bc_pattern)
    cell_barcode_counts = collections.Counter()
    n_reads = 0
    n_cell_barcodes = 0
    for read1 in read1s:
        n_reads += 1
        barcode_values = ReadExtractor.getBarcodes(read1)
        if barcode_values is None:
            continue
        else:
            cell, umi, _, _, _, _, _ = barcode_values
            cell_barcode_counts[cell] += 1
            n_cell_barcodes += 1
    logging.info("Parsed %d reads", n_reads)
    cell_whitelist, true_to_false_map = whitelist_methods.getCellWhitelist(
        cell_barcode_counts,
        whitelist_knee_method, # knee_method: density or distance
        False, # expect_cells
        False, # cell_number
        1, # error_correct_threshold
        None # plot_prefix
        )
    if ed_above_threshold:
        cell_whitelist, true_to_false_map = whitelist_methods.errorDetectAboveThreshold(
            cell_barcode_counts,
            cell_whitelist,
            true_to_false_map,
            errors = 1, # error_correct_threshold,
            resolution_method = ed_above_threshold)
    # Write whitelist file
    wlist_out = openFile(out_r2.replace("UMIextract.fastq.gz", "whitelist.txt"), "w")
    total_correct_barcodes = 0
    total_corrected_barcodes = 0
    for barcode in sorted(list(cell_whitelist)):
        total_correct_barcodes += cell_barcode_counts[barcode]
        if true_to_false_map:
            corrected_barcodes = ",".join(sorted(true_to_false_map[barcode]))
            correct_barcode_counts = [cell_barcode_counts[x] for x in sorted(true_to_false_map[barcode])]
            total_corrected_barcodes += sum(correct_barcode_counts)
            corrected_barcode_counts = ",".join(map(str, correct_barcode_counts))
        else:
            corrected_barcodes, corrected_barcode_counts = "", ""
        wlist_out.write("%s\t%s\t%s\t%s\n" % (barcode, corrected_barcodes, cell_barcode_counts[barcode], corrected_barcode_counts))
    return cell_whitelist, true_to_false_map

######################################################################
## Extract Single-Cell UMI and Cellular barcode from R1 / R2 files. ##
######################################################################
def extractUMI(in_r1, in_r2, bc_pattern, out_r2, whitelist, whitelist_knee_method):
    logging.info("UMI Extraction...")
    # set up read extractor
    ReadExtractor = extract_methods.ExtractFilterAndUpdate(
        method = "string",
        pattern = bc_pattern)

    if whitelist == "":
        logging.info("No whitelist provided, computing it.")
        cell_wlist, f_t_map = computeWhitelist(in_r1, bc_pattern, out_r2, whitelist_knee_method)
    else:
        cell_wlist, f_t_map = whitelist_methods.getUserDefinedBarcodes(whitelist, getErrorCorrection = False)
    ReadExtractor.cell_whitelist = cell_wlist
    ReadExtractor.false_to_true_map = f_t_map

    read1s = umi_methods.fastqIterate(openFile(in_r1))
    read2s = umi_methods.fastqIterate(openFile(in_r2))
    read2_out = openFile(out_r2, "w")
    progCount = 0
    displayMax = 500000
    for read1, read2 in umi_methods.joinedFastqIterate(read1s, read2s):
        # incrementing count for monitoring progress
        progCount += 1
        # Update display in every 100kth iteration
        if progCount % displayMax == 0:
            logging.info("Parsed %d reads", progCount)
            sys.stdout.flush()
        reads = ReadExtractor(read1, read2)
        if not reads:
            continue
        else:
            new_read1, new_read2 = reads
        read2_out.write(str(new_read2) + "\n")
    read2_out.close()
    logging.info("UMI Extraction Completed!")
    return 0

#######################################
## Save BAR-seq outputs for a sample ##
## Prepare TSV files and plots       ##
#######################################
def save_barseq_outputs(npa, bcode_merge, bcode_umi, bcode_len, sample_lab, out_dir, min_count, sum_counts, sc_mode):
    full_out_name = out_dir + "/" + sample_lab + ".barcode.mc" + str(min_count) + ".tsv"
    full_out = open(full_out_name, "w")
    if sc_mode:
        full_out.write("\t".join([sample_lab + "-" + x for x in ["Barcode", "Count", "SaturationPerc", "CBs", "CBCounts"]]) + "\n")
    else:
                full_out.write("\t".join([sample_lab + "-" + x for x in ["Barcode", "Count", "SaturationPerc"]]) + "\n")
    whole_bcode_compos = [{'A' : 0, 'C' : 0, 'G' : 0, 'T' : 0} for k in range(bcode_len)]
    sum_counts_cumul = 0
    ss = 0
    bcode_counts = []
    bcode_saturations = []
    sel_sat = [25, 50, 75]
    num_sel = 0
    num_selected = {}
    for npa_elem in npa:
        sum_counts_cumul += npa_elem['count']
        ss = (sum_counts_cumul / sum_counts) * 100
        if sc_mode:
            cbs = [x for x in bcode_umi[npa_elem['barcode']].keys()]
            cbs_counts = [str(len(bcode_umi[npa_elem['barcode']][x])) for x in cbs]
            full_out.write("\"" + npa_elem['barcode'] + "\"\t" + str(npa_elem['count']) + "\t" + "{:.2f}".format(round(ss, 2)) + "\t" + ",".join(cbs) + "\t" + ",".join(cbs_counts) + "\n")
        else:
            full_out.write("\"" + npa_elem['barcode'] + "\"\t" + str(npa_elem['count']) + "\t" + "{:.2f}".format(round(ss, 2)) + "\n")
        logging.debug("%s\t%d (%d)\t%.2f", npa_elem['barcode'], npa_elem['count'], sum_counts_cumul, ss)
        bcode_counts.append(npa_elem['count'])
        bcode_saturations.append(ss)
        for p in range(bcode_len):
            if npa_elem['barcode'][p] in whole_bcode_compos[p]:
                whole_bcode_compos[p][npa_elem['barcode'][p]] += npa_elem['count']
        if num_sel < len(sel_sat) and ss > sel_sat[num_sel]:
            num_selected[sel_sat[num_sel]] = len(bcode_counts)
            num_sel += 1
    # Prepare plots
    logo_df = pd.DataFrame(whole_bcode_compos)
    logo = lm.Logo(logo_df, font_name = 'DejaVu Sans')
    plt.savefig(out_dir + "/" + sample_lab + ".barcode.structure.png")
    ranks = np.arange(1, len(npa) + 1, 1)
    hline = {sat : [sat for x in range(len(npa))] for sat in sel_sat}
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_xlabel('Rank')
    ax1.set_ylabel('Counts', color = color)
    ax1.plot(ranks, bcode_counts, color = color)
    ax1.tick_params(axis = 'y', labelcolor = color)
    for vl in num_selected:
        plt.axvline(x = num_selected[vl], linestyle = "--", color = color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('Saturation (%)', color = color)  # we already handled the x-label with ax1
    ax2.plot(ranks, bcode_saturations, color = color)
    for hl in hline:
        ax2.plot(ranks, hline[hl], "--", color = color)
    ax2.tick_params(axis = 'y', labelcolor = color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(out_dir + "/" + sample_lab + ".barcode.saturation.png")
    return 0

#######################################################
## Filter barcodes based on (inferred) length        ##
## and on different filters: nofilter, percfilter,   ##
## and fixedstruct (which requires a IUPAC structure ##
#######################################################
def filter_barcodes(bcode_fq, bcode_lens, filter_struct, iupac_struct):
    bcode_len =  max(bcode_lens, key = bcode_lens.get)
    logging.info("Barcode length: %d", bcode_len)
    if filter_struct == "fixedstruct" and len(iupac_struct) < bcode_len:
        iupac_struct = iupac_struct + ''.join(["N" for x in range(bcode_len - len(iupac_struct))])
        logging.warning("IUPAC code %s is shorter than barcode length!")
        logging.warning("Adding N chracters: %s", iupac_struct)
    # Check barcode lengths and structures
    whole_bcode_compos = [{'A' : 0, 'C' : 0, 'G' : 0, 'T' : 0, 'sum' : 0} for k in range(bcode_len)]
    for bcode_seq in bcode_fq.keys():
        if len(bcode_seq) != bcode_len:
            continue
        for p in range(bcode_len):
            if bcode_seq[p] not in ['A', 'C', 'G', 'T']:
                continue
            whole_bcode_compos[p][bcode_seq[p]] += bcode_fq[bcode_seq]
            whole_bcode_compos[p]['sum'] += bcode_fq[bcode_seq]
    # Filter barcodes by length and structure
    bcode_filter = {}
    bcode_content = {}
    for bcode_seq in bcode_fq.keys():
        if len(bcode_seq) != bcode_len:
            continue
        # No structural filter
        if filter_struct == "nofilter":
            bcode_filter[bcode_seq] = bcode_fq[bcode_seq]
            bcode_content[bcode_seq] = [bcode_seq.count('A'), bcode_seq.count('C'),
                                        bcode_seq.count('T'), bcode_seq.count('T')]
        # Filter based on nucleotide percentage
        elif filter_struct == "percfilter":
            wrong_pos = 0
            for p in range(bcode_len):
                if bcode_seq[p] not in ['A', 'C', 'G', 'T']:
                    continue
                if (whole_bcode_compos[p][bcode_seq[p]] / whole_bcode_compos[p]['sum']) * 100 < 1:
                    wrong_pos += 1
            if wrong_pos == 0:
                bcode_filter[bcode_seq] = bcode_fq[bcode_seq]
                bcode_content[bcode_seq] = [bcode_seq.count('A'), bcode_seq.count('C'),
                                            bcode_seq.count('T'), bcode_seq.count('T')]
        # Filter based on fixed structure (IUPAC)
        elif filter_struct == "fixedstruct":
            if match_bc(bcode_seq, iupac_struct) == 0:
                bcode_filter[bcode_seq] = bcode_fq[bcode_seq]
                bcode_content[bcode_seq] = [bcode_seq.count('A'), bcode_seq.count('C'),
                                            bcode_seq.count('T'), bcode_seq.count('T')]
        # Should never reach this point
        else:
            logginng.error("Wrong structural filter")
            return 1
    logging.info("Barcodes after filter: %d", len(bcode_filter))
    return bcode_filter, bcode_len

#########################################################
## Compute saturation values for the computed barcodes ##
#########################################################
def compute_saturation(bcode_merge, bcode_len, min_count):
    dtype = [('barcode', np.unicode_, bcode_len), ('count', int)]
    vals = []
    tot_count = 0
    for barcode in bcode_merge.keys():
        count = bcode_merge[barcode]
        tot_count += count
        # Filter out barcodes having count less than min_count
        if int(count) >= min_count:
            vals.append((barcode, count))
    # Create numpy array
    npa = np.array(vals, dtype = dtype)
    logging.info("Sum of all barcode counts %d", tot_count)
    # Sort barcodes based on counts
    npa = np.sort(npa, order = 'count')[::-1]
    return npa

###############################################
## Compute edit distances among all the BARs ##
###############################################
def compute_edit_dist(bc1, num_threads):
    logging.info("Computing edit distances among barcodes")
    m = len(bc1)
    dm = np.zeros((m * (m - 1)) // 2, dtype = np.int8)
    if num_threads == 1 or m < 10:
        mx = 0
        for bx in range(0, m - 1):
            for by in range(bx + 1, m):
                dm[mx] = ed.eval(bc1[bx], bc1[by])
                mx += 1
    else:
        split_array = splitRange(len(dm), num_threads)
        with Pool(num_threads) as pool:
            func = partial(computeED, bc1)
            for res in pool.map(func, split_array):
                dm += res
    return dm

###################################################
## Correct BAR counts based on Single-cell + UMI ##
## sequences from read ID                        ##
###################################################
def correct_counts(bcode_seq, bcode_ids, out_dir, sample_lab):
    bcode_umi = {}
    bcode_seq_filt = {}
    with open(out_dir + "/" + sample_lab + "_initial_quantif.csv", "w") as quant_file:
        x = []
        y = []
        for bcode in bcode_seq.keys():
            bcode_quant = {}
            for bcode_id in bcode_ids[bcode]:
                rname,cb,umi = (bcode_id.split(" ")[0]).split("_")
                if cb not in bcode_quant:
                    bcode_quant[cb] = {}
                if umi not in bcode_quant[cb]:
                    bcode_quant[cb][umi] = 0
                bcode_quant[cb][umi] += 1
            # Remove single UMI with (read) count 1
            rem_cb = []
            for cb in bcode_quant:
                bcode_quant[cb] = {key : val for key,val in bcode_quant[cb].items() if val > 1}
                if len(bcode_quant[cb]) == 0:
                    rem_cb.append(cb)
            # Add here additional filters on BC/UMI
            # ...
            for cb in rem_cb:
                del bcode_quant[cb]
            # Count UMI
            ss = sum([len(bcode_quant[x]) for x in bcode_quant.keys()])
            if ss > 0:
                quant_file.write(bcode + "," + str(bcode_seq[bcode]) + "," + str(ss) + "\n")
                x.append(bcode_seq[bcode])
                y.append(ss)
                # Correct quantification
                bcode_seq_filt[bcode] = ss
                bcode_umi[bcode] = bcode_quant
        fig, ax1 = plt.subplots()
        color = 'tab:red'
        ax1.set_xlabel('Num. Reads')
        ax1.set_ylabel('Num. UMIs')
        plt.scatter(x, y, alpha = 0.5)
        plt.savefig(out_dir + "/" + sample_lab + "_initial_quantif.png")
    logging.info("Number of barcodes after requantification: %d", len(bcode_seq_filt))
    return bcode_seq_filt, bcode_umi

###########################################################
## Remove BAR having (corrected) count less than 'min_c' ##
###########################################################
def remove_count_one(bcode_seq, min_c=2):
    logging.info("Removing barcodes with count less than %d", min_c)
    bcode_filtered = {}
    bcode_single = {}
    for bcode in bcode_seq:
        if bcode_seq[bcode] <= min_c:
            bcode_single[bcode] = bcode_seq[bcode]
        else:
            bcode_filtered[bcode] = bcode_seq[bcode]
    logging.info("Barcodes with count <= %d: %d (removed)", min_c, len(bcode_single))
    logging.info("Barcodes with count > %d: %d (kept)", min_c, len(bcode_filtered))
    return bcode_filtered,bcode_single

###################################################
## Graph-based procedure to merge barcodes with  ##
## edit distance lower than a given threshold    ##
###################################################
def merge_barcodes(g, threshold, bcode_ids, bcode_umi, sc_mode, correct_w_cbs = True):
    seqs = np.array([g.nodes[x]['seq'] for x in g.nodes()])
    if len(seqs) > 1:
        # print("-------START_CC-------")
        # print("\n".join([g.nodes[x]['seq'] for x in g.nodes()]))
        data = {'counts' : [g.nodes[x]['counts'] for x in g.nodes()], 'seq' : [g.nodes[x]['seq'] for x in g.nodes()] }
        data_df = pd.DataFrame(data, index = [x for x in g.nodes()], columns = ['seq', 'counts']).sort_values(by=['counts'], ascending = False)
        merged = {}
        while len(data_df.index) > 0:
            ego_nodes = list(nx.ego_graph(g, data_df.index[0], threshold, distance = 'weight').nodes)
            ego_nodes = [x for x in ego_nodes if x in data_df.index]
            # print(ego_nodes)
            rep_seq = data_df.loc[data_df.index[0], 'seq']
            if sc_mode:
                rep_cbs = list(bcode_umi[rep_seq].keys())
                if correct_w_cbs:
                    ego_nodes = [eg for eg in ego_nodes if len(np.intersect1d(rep_cbs, list(bcode_umi[data_df.loc[eg, 'seq']].keys()), assume_unique = True)) > 0]
            # print(" - ".join([str(x) + "_" + g.nodes[x]['seq'] for x in ego_nodes]))
            merged[rep_seq] = data_df.loc[ego_nodes, 'counts'].sum()
            excl_nodes = [x for x in data_df.index if not x in ego_nodes]
            for x in list(data_df.loc[ego_nodes, 'seq']):
                if x != rep_seq:
                    bcode_ids[rep_seq] += bcode_ids[x]
                    bcode_ids[x] = list()
                    if sc_mode:
                        for cb in bcode_umi[x]:
                            if cb not in bcode_umi[rep_seq]:
                                bcode_umi[rep_seq][cb] = {}
                            for umi in bcode_umi[x][cb]:
                                if umi not in bcode_umi[rep_seq][cb]:
                                    bcode_umi[rep_seq][cb][umi] = 0
                                bcode_umi[rep_seq][cb][umi] += bcode_umi[x][cb][umi]
                        bcode_umi[x] = {}
            data_df = data_df.loc[excl_nodes, :].reindex(index = excl_nodes, columns = ['seq', 'counts']).sort_values(by=['counts'], ascending = False)
        # print("-------END_CC-------")
        return(merged)
    else:
        return({seqs[0] : g.nodes[list(g.nodes())[0]]['counts']})

######################################################
## NEW graph-based procedure to merge barcodes with ##
## edit distance lower than a given threshold using ##
## community detection procedure                    ##
######################################################
def merge_barcodes_communities(g, bcode_ids, bcode_umi, sc_mode):
    seqs = np.array([g.nodes[x]['seq'] for x in g.nodes()])
    if len(seqs) > 1:
        merged = {}
        communities = greedy_modularity_communities(g, weight = "abundance")
        for comm in communities:
            data = {'counts' : [g.nodes[x]['counts'] for x in comm], 'seq' : [g.nodes[x]['seq'] for x in comm] }
            data_df = pd.DataFrame(data, index = [x for x in comm], columns = ['seq', 'counts']).sort_values(by=['counts'], ascending = False)
            rep_seq = data_df.loc[data_df.index[0], 'seq']
            merged[rep_seq] = data_df['counts'].sum()
            for x in list(data_df['seq']):
                if x != rep_seq:
                    bcode_ids[rep_seq] += bcode_ids[x]
                    bcode_ids[x] = list()
                    if sc_mode:
                        for cb in bcode_umi[x]:
                            if cb not in bcode_umi[rep_seq]:
                                bcode_umi[rep_seq][cb] = {}
                            for umi in bcode_umi[x][cb]:
                                if umi not in bcode_umi[rep_seq][cb]:
                                    bcode_umi[rep_seq][cb][umi] = 0
                                bcode_umi[rep_seq][cb][umi] += bcode_umi[x][cb][umi]
                        bcode_umi[x] = {}
        return(merged)
    else:
        return({seqs[0] : g.nodes[list(g.nodes())[0]]['counts']})

##########################################
## Extract barcode sequences from input ##
## reads using TagDust software         ##
##########################################
def extract_barcodes(in_fq, out_dir, sample_lab, td_prog, td_opt, num_threads=1):
    # Check previous TagDust runs
    if os.path.exists(out_dir + "/" + sample_lab + ".fq"):
        os.remove(out_dir + "/" + sample_lab + ".fq")
    if os.path.exists(out_dir + "/" + sample_lab + "_un.fq"):
        os.remove(out_dir + "/" + sample_lab + "_un.fq")
    if os.path.exists(out_dir + "/" + sample_lab + "_logfile.txt"):
        os.remove(out_dir + "/" + sample_lab + "_logfile.txt")
    td_cmd = td_prog + " " + td_opt + " -t " + str(num_threads) + " -o " + out_dir + "/" + sample_lab + " " + in_fq
    logging.debug(td_cmd)
    exit;
    stream = os.popen(td_cmd)
    output = stream.read().rstrip()

    # Parse TagDust results
    td_res = out_dir + "/" + sample_lab + ".fq"
    if not os.path.exists(td_res):
        logging.error("TagDust results %s not found!", td_res)
        sys.exit(1)
    bcode_fq = {} # Store BARs sequences as keys and count the different found ones
    bcode_lens = {} # Store BARs lengths as keys and count the different (possible) found ones
    bcode_ids = {} # Store BARs sequences as keysStore and FASTQ id of each BAR
    with open(td_res, 'r') as in_fq:
        lines = []
        for line in in_fq:
            lines.append(line.rstrip())
            if len(lines) == 4:
                record = processFQ(lines)
                bcode_seq = record['sequence']
                bcode_name = record['name']
                if len(bcode_seq) not in bcode_lens:
                    bcode_lens[len(bcode_seq)] = 0
                bcode_lens[len(bcode_seq)] += 1
                if bcode_seq not in bcode_fq:
                    bcode_fq[bcode_seq] = 0
                    bcode_ids[bcode_seq] = []
                bcode_fq[bcode_seq] += 1
                bcode_ids[bcode_seq].append(bcode_name)
                lines = []
    logging.info("Number of input read ids: %d", sum(len(bcode_ids[id]) for id in bcode_ids))
    logging.info("Read %d distinct barcodes from %s", len(bcode_fq), td_res)
    # gzip TagDust outputs
    if os.path.exists(out_dir + "/" + sample_lab + ".fq"):
        with open(out_dir + "/" + sample_lab + ".fq", 'rb') as in_fq:
            with gzip.open(out_dir + "/" + sample_lab + ".fq.gz", 'wb') as f_out:
                shutil.copyfileobj(in_fq, f_out)
        os.remove(out_dir + "/" + sample_lab + ".fq")
    if os.path.exists(out_dir + "/" + sample_lab + "_un.fq"):
        with open(out_dir + "/" + sample_lab + "_un.fq", 'rb') as in_fq:
            with gzip.open(out_dir + "/" + sample_lab + "_un.fq.gz", 'wb') as f_out:
                shutil.copyfileobj(in_fq, f_out)
        os.remove(out_dir + "/" + sample_lab + "_un.fq")
    return bcode_fq, bcode_lens, bcode_ids

#################################################
## Perform BAR-seq analysis on a single sample ##
#################################################
def barseq(in_fq, sample_lab, out_dir, td_prog, td_opt, filter_struct, iupac_struct,
           ed_thr, min_count, sc_mode, merge_algorithm, num_threads=1, save_graph=False, log_level=logging.INFO):
    logging.root.handlers = []
    logging.basicConfig(level = log_level,
                        format = '[%(asctime)s] %(levelname)-8s %(message)s',
                        datefmt = "%Y-%m-%d %H:%M:%S",
                        handlers = [logging.FileHandler(out_dir + "/" + sample_lab + "_barseq.log"),
                                    logging.StreamHandler()])
    logging.info("## Running BAR-seq on sample %s", sample_lab)

    # Extract barcodes
    bcode_seq,bcode_lens,bcode_ids = extract_barcodes(in_fq, out_dir, sample_lab, td_prog, td_opt, num_threads)

    # Filter barcodes by length and structure
    bcode_seq,bcode_len = filter_barcodes(bcode_seq, bcode_lens, filter_struct, iupac_struct)
    for bc in set(bcode_ids) - set(bcode_seq):
        del bcode_ids[bc]
    logging.debug("Number of read ids after filter: %d", sum(len(bcode_ids[id]) for id in bcode_seq))

    # Filter (save in another structure) barcodes with count 1 read
    bcode_seq,bcode_single = remove_count_one(bcode_seq)

    # Correct quantification based on Cellular Barcodes + UMIs (and filter)
    bcode_umi = {}
    if sc_mode:
        bcode_seq,bcode_umi = correct_counts(bcode_seq, bcode_ids, out_dir, sample_lab)

    # Graph-based merging: compute edit distances
    bc1 = np.array(list(bcode_seq.keys()))
    if len(bc1) > 1:
        dm = compute_edit_dist(bc1, num_threads)
        if sum(dm) == 0:
            logging.error("Error in computing edit distances among BARs.")
            return 1
    # Graph-based merging: build graph
    logging.info("Building graph")
    G = nx.Graph()
    l_bc = len(bc1)
    G.add_nodes_from(np.arange(l_bc))
    for x in G.nodes():
        G.nodes[x]['seq'] = str(bc1[x])
        G.nodes[x]['counts'] = bcode_seq[bc1[x]]
    if len(bc1) > 1:
        e_mask = dm <= ed_thr
        e_idx = [condensed_to_square(x, l_bc) for x in np.where(e_mask)[0]]
        e_w = dm[e_mask]
        G.add_edges_from(e_idx)
        for x, e in enumerate(e_idx):
            G.edges[e[0], e[1]]['weight'] = e_w[x]
            e_abundance = G.nodes[e[0]]['counts'] + G.nodes[e[1]]['counts']
            G.edges[e[0], e[1]]['abundance'] = e_abundance
    if save_graph:
        G_copy = G.copy()
        if sc_mode:
            for x in G_copy.nodes():
                G_copy.nodes[x]['cbs'] = ",".join([cb + ":" + str(len(bcode_umi[bc1[x]][cb])) for cb in list(bcode_umi[bc1[x]].keys())])
        graph_name = out_dir + "/" + sample_lab + ".graph_full.graphml"
        nx.write_graphml(G_copy, graph_name)

    logging.info("Merging barcodes with edit distance lower than %d", ed_thr)
    bcode_seq = {}
    for cc in nx.connected_components(G):
        # slow but...
        g = nx.subgraph(G, cc)
        if merge_algorithm == "ego":
            bcode_seq.update(merge_barcodes(g, ed_thr, bcode_ids, bcode_umi, sc_mode, correct_w_cbs = True))
        elif merge_algorithm == "communities":
            bcode_seq.update(merge_barcodes_communities(g, bcode_ids, bcode_umi, sc_mode))
    logging.info("Barcodes after merge: %d", len(bcode_seq))

    # Compute saturation
    npa = compute_saturation(bcode_seq, bcode_len, min_count)

    # Sum barcode counts
    sum_counts = float(np.sum(npa[:]['count']))
    logging.info("Barcodes having count greater than %d: %d", min_count, len(npa))
    logging.info("Sum of barcodes having count greater than %d: %.0f", min_count, sum_counts)

    # Print results
    save_barseq_outputs(npa, bcode_seq, bcode_umi, bcode_len, sample_lab, out_dir, min_count, sum_counts, sc_mode)
    logging.info("## BAR-seq on sample %s finished!", sample_lab)
    return 0

############
##  Main  ##
############

msg_logo = r'''
  ____            _____                        
 |  _ \    /\    |  __ \                       
 | |_) |  /  \   | |__) |     ___   ___   __ _ 
 |  _ <  / /\ \  |  _  / ___ / __| / _ \ / _` |
 | |_) |/ ____ \ | | \ \     \__ \|  __/| (_| |
 |____//_/    \_\|_|  \_\    |___/ \___| \__, |
                                            | |
                                            |_|
'''

def main(raw_args = None):
    parser = argparse.ArgumentParser(prog = "runBARseq",
                                     description = "Run BAR-seq analysis pipeline.",
                                     formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-r1", "--r1-files", help = "Comma separated list of R1 files.",
                        required = True, dest = "in_r1_files")
    parser.add_argument("-r2", "--r2-files", help = "Comma separated list of R2 files (single-cell mode).",
                        dest = "in_r2_files", default = "")
    parser.add_argument("-bc", "--bc-pattern", help = "Barcode pattern (single-cell mode).",
                        dest = "bc_pattern", default = "")
    parser.add_argument("-l", "--file-labels", help = "Comma separated list of file labels.",
                        dest = "in_labels", default = "")
    parser.add_argument("-o", "--out-dir", help = "Output directory (must exist).",
                        dest = "out_dir", default = ".")
    parser.add_argument("-wl", "--whitelist", help = "A whitelist of accepted cell barcodes (single-cell mode).",
                        dest = "whitelist", default = "")
    parser.add_argument("--wl-knee-method", "-wl-knee-method", help = "Use distance or density methods for detection of knee in whitelist computation (single-cell mode).",
                        dest = "whitelist_knee_method", choices = ["distance", "density"], default = "density")
    parser.add_argument("-trim-sequences", "--trim-sequences", help = "Trim input R2 sequences with 'cutadapt'.",
                        dest = "trim_seq", action = "store_true")
    parser.add_argument("-trim-opt", "--trim-opt", help = "Cutadapt parameters used to trim input R2 sequences.",
                        dest = "trim_opt", default = "")
    parser.add_argument("-tagdust-dir", "--tagdust-dir", help = "TagDust program directory.",
                        dest = "tagdust_dir", default = "")
    parser.add_argument("-tagdust-opt", "--tagdust-opt", help = "TagDust run options (e.g. \"-1 P:CAGGG -2 R:N -3 S:TAGGGACAGGA -4 P:AAGCCTCCTCCT\").",
                        dest = "tagdust_opt", default = "")
    parser.add_argument("-f", "--filter-struct", help = "Structural filter: 'nofilter, 'percfilter', or 'fixedstruct'.",
                        dest = "filter_struct", default = "percfilter")
    parser.add_argument("-iupac", "--iupac-struct", help = "IUPAC structure used only with 'fixedstruct' filter.",
                        dest = "iupac_struct", default = "N")
    parser.add_argument("-e", "--edit-distance", help = "Edit distance threshold for graph-based merging.",
                        dest = "ed_thr", type = int, default = 2)
    parser.add_argument("-merge-algorithm", "--merge-algorithm", help = "Graph Algorithm for Barcode mergins: 'communities' (new) or 'ego' (old).",
                        dest = "merge_algorithm", default = "communities")
    parser.add_argument("-c", "--min-count", help = "Minimum count threshold for an input barcode.",
                        dest = "min_count", default = 3, type = int)
    parser.add_argument("-t", "--threads", help = "Num. of threads used in sequence comparisons.",
                        dest = "threads", default = 1, type = int)
    parser.add_argument("-save-graph", "--save-graph", help = "Save graph used to correct barcodes (GraphML format).",
                        dest = "save_graph", action = "store_true")
    parser.add_argument("-v", "--verbose", help = "Increase verbosity.",
                        action = "count", default = 0)
    args = parser.parse_args(raw_args)

    if args.verbose == 0:
        log_level = logging.INFO
    elif args.verbose == 1:
        log_level = logging.DEBUG
    else:
        log_level = logging.DEBUG

    logging.basicConfig(level = log_level,
                        format = "[%(asctime)s] %(levelname)-8s %(message)s",
                        datefmt = "%Y-%m-%d %H:%M:%S")
    logging.info(msg_logo)
    logging.info("### BAR-seq Started ###")
    sc_mode = False
    if args.in_r2_files != "":
        sc_mode = True

    # Check input files and labels
    if sc_mode:
        if len(args.in_r1_files.split(",")) != len(args.in_r2_files.split(",")):
            logging.error("Input R1/R2 files do not match.")
            return 1
    input_r1_files = args.in_r1_files.split(",")
    if sc_mode:
        input_r2_files = args.in_r2_files.split(",")
    else:
        # Add 'fake R2'
        input_r2_files = args.in_r1_files.split(",")
    input_labels = []
    if args.in_labels != "":
        input_labels = args.in_labels.split(",")
        if len(input_r1_files) != len(input_labels):
            logging.error("Labels do not match input files!")
            return 1
    else:
        for in_fq_file in input_r1_files:
            # Infer sample label
            sample_lab = in_fq_file.split("/")[-1]
            sample_lab = sample_lab.replace(".gz", "")
            sample_lab = sample_lab.replace(".fq", "")
            sample_lab = sample_lab.replace(".fastq", "")
            sample_lab = sample_lab.replace("_R1_001", "")
            sample_lab = sample_lab.replace("_R1", "")
            input_labels.append(sample_lab)
    input_files = {}
    for in_samp in range(0, len(input_labels)):
        input_files[input_labels[in_samp]] = {"R1" : input_r1_files[in_samp], "R2" : input_r2_files[in_samp]}
    logging.debug(input_files)

    # Check output directory
    if not os.path.exists(args.out_dir):
        logging.error("Output directory %s not found!", args.out_dir)
        return 1
    # Check filter
    if args.filter_struct not in ["nofilter", "percfilter", "fixedstruct"]:
        logging.error("Wrong structural filter: %s", args.filter_struct)
        logging.error("Allowed filters: 'nofilter', 'percfilter', or 'fixedstruct'.")
        return 1
    if args.filter_struct == "fixedstruct" and args.iupac_struct == "":
        logging.warning("Empty IUPAC code reuqired for 'fixedstruct' filter")
        logging.warning("If you want to specify it use the -iupac option")
    # Check Graph Merge Algorithm
    if args.merge_algorithm not in ["communities", "ego"]:
        logging.error("Wrong graph merging algorithm: %s", args.merge_algorithm)
        logging.error("Allowed algorithms: 'communities' or 'ego'.")
        return 1
    # TagDust
    td_prog = checkTagDustProg(args.tagdust_dir)
    if td_prog == "":
        logging.error("Could not locate 'tagdust' program! Try to set the path with '--tagdust-dir'.")
        return 1
    # Cutadapt
    if args.trim_seq:
        check_program("cutadapt")
        if args.trim_opt == "":
            logging.error("Cutadapt trimmming options '-trim-opt' are missing!")
            return 1
        else:
            if "-o" in args.trim_opt:
                logging.error("Option '-o' provided to cutadapt! Remove it from '-trim-opt'.")
                return 1
    # Run BAR-seq on all the samples
    for sample in input_files:
        logging.info("## Pre-process Sample: %s", sample)
        # Check input FQ file
        if not (os.path.exists(input_files[sample]["R1"]) and os.path.exists(input_files[sample]["R2"])):
            logging.error("Input FASTQ file(s) not found: %s", " / ".join(list(input_files[sample].values())))
            return 1
        r1_file = input_files[sample]["R1"]
        r2_file = input_files[sample]["R2"]
        # Trim sequences
        if args.trim_seq:
            r1_file_trim, r2_file_trim = trim_reads(r1_file, r2_file, args.trim_opt, args.out_dir, sample, args.bc_pattern, sc_mode, args.threads)
            r1_file = r1_file_trim
            r2_file = r2_file_trim
        # Extract UMI from R1/R2 (using whitelist)
        out_umi_extract = r1_file
        if sc_mode:
            out_umi_extract = args.out_dir + "/" + sample + "_UMIextract.fastq.gz"
            if extractUMI(r1_file, r2_file, args.bc_pattern, out_umi_extract, args.whitelist, args.whitelist_knee_method) != 0:
                logging.error("Problem in extracting UMI/Cellular Barcodes from %s.", " / ".join(list(input_files[sample].values())))
                continue
        # Guess amplicon structure for TagDust (if needed)
        td_opt = args.tagdust_opt
        if td_opt == "":
            td_opt = guessTagDustOpt(out_umi_extract)
        if barseq(out_umi_extract, sample, args.out_dir, td_prog, td_opt, args.filter_struct,
                  args.iupac_struct, args.ed_thr, args.min_count, sc_mode, args.merge_algorithm,
                  args.threads, args.save_graph, log_level) != 0:
            logging.error("Error in running BAR-seq on sample: %s", out_umi_extract)
            return 1
    logging.root.handlers = []
    logging.basicConfig(level = log_level,
                        format = "[%(asctime)s] %(levelname)-8s %(message)s",
                        datefmt = "%Y-%m-%d %H:%M:%S",
                        handlers = [logging.StreamHandler()]
                        )
    # Compute Sharing
    if len(input_files) > 1:
        logging.info("-----------------------")
        logging.info("Compute Barcode Sharing")
        full_df = pd.DataFrame()
        for lab in input_labels:
            td_res = args.out_dir + "/" + lab + ".barcode.mc" + str(args.min_count) + ".tsv"
            if not os.path.exists(td_res):
                continue
            df = pd.read_csv(td_res, sep = "\t")
            df.columns = ["Barcode", lab, "Saturation"]
            df = df[["Barcode", lab]]
            if len(full_df.index) == 0:
                full_df = df
            else:
                full_df = pd.merge(full_df, df, on = "Barcode", how = "outer")
        full_df = full_df.fillna(0)
        full_df = full_df.sort_values(by = ["Barcode"])
        full_df.to_csv(args.out_dir + "/Barcode.mc" + str(args.min_count) + "_fullMatrix.tsv",
                       index = False, header = True, sep = "\t", float_format="%.0f")
    logging.info("### BAR-seq finished ###")
    return 0

if __name__ == "__main__":
    main()
